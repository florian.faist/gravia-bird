package at.gravia.bird.data;

import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

class MailDtoTest {

    @Test
    void validate() {
        MailDto mailDto = new MailDto();
        mailDto.setTo("flo.exabyte@gmail.com");
        mailDto.setFrom("service@gravia.at");
        mailDto.setBetreff("betreff");
        mailDto.setContent("simple test");
        mailDto.setHtmlContent(false);
        Assert.assertTrue(mailDto.validate());

    }

    @Test
    void checkAttachmentToByteArray() {
        MailDto mailDto = new MailDto();
        mailDto.setTo("flo.exabyte@gmail.com");
        mailDto.setFrom("service@gravia.at");
        mailDto.setContent("simple test");
        mailDto.setBetreff("betreff");
        mailDto.setHtmlContent(false);
        Attachment attachment = new Attachment();
        attachment.setName("test.txt");
        attachment.setData("someNiceTest".getBytes());
        Assert.assertEquals("someNiceTest", new String(attachment.getData()));
        mailDto.addAttachment(attachment);
        Gson gson = new Gson();
        System.out.println(gson.toJson(mailDto));

    }
}