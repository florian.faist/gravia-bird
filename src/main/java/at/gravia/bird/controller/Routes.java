package at.gravia.bird.controller;

public class Routes {
    public static final String SEND_MAIL = "/bird/sendmail";
    public static final String HTML_TO_PDF = "/bird/htmltopdf";
}
