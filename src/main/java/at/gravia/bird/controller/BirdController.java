package at.gravia.bird.controller;


import at.gravia.bird.data.HtmlDto;
import at.gravia.bird.data.MailDto;
import at.gravia.bird.exception.BirdException;
import at.gravia.bird.service.HtmlToPdfService;
import at.gravia.bird.service.SendMail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import static at.gravia.bird.controller.Routes.HTML_TO_PDF;
import static at.gravia.bird.controller.Routes.SEND_MAIL;

@RestController
@Slf4j
public class BirdController {

    @Value("${server.api.key}")
    private String apiKey;
    @Autowired
    private SendMail sendMail;
    @Autowired
    private HtmlToPdfService htmlToPdfService;

    @RequestMapping(value = SEND_MAIL, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> sendMail(
            @RequestHeader("api-key") String headerApiKey,
            @RequestBody MailDto mailDto) {
        if (!isApiKeyValid(headerApiKey)) {
            return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
        }
        try {
            mailDto.validate();
        } catch (BirdException e) {
            return ResponseEntity.badRequest().body(e.getMessageFormated());
        }
        checkAndConvertHtmlAttachmentToPdfIfNeeded(mailDto);
        sendMail.sendMail(mailDto);
        log.info("Mail send to " + mailDto.getTo() + " from " + mailDto.getFrom());
        return ResponseEntity.ok().build();
    }


    @RequestMapping(value = HTML_TO_PDF, method = RequestMethod.POST, consumes = {"application/json"}, produces = {"application/pdf"})
    public ResponseEntity<byte[]> createPdfFromHtml(
            @RequestHeader("api-key") String headerApiKey,
            @RequestBody HtmlDto htmlDto) {
        if (!isApiKeyValid(headerApiKey)) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
        byte[] pdfbytes = htmlToPdfService.generatePDF(htmlDto);
        if (pdfbytes != null) {
            return new ResponseEntity(pdfbytes, headers, HttpStatus.OK);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public void checkAndConvertHtmlAttachmentToPdfIfNeeded(MailDto mailDto) {
        if (mailDto != null && mailDto.getAttachments() != null) {
            mailDto.getAttachments().forEach(attachment -> {
                if (attachment.isDataAsHtmlToPdf()) {
                    attachment.setData(htmlToPdfService.generatePDFFromString(attachment.getDataAsString()));
                    attachment.setDataAsHtmlToPdf(false);
                }
            });
        }
    }

    private boolean isApiKeyValid(String key) {
        return key.equals(this.apiKey);
    }
}