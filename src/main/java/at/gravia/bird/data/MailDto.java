package at.gravia.bird.data;

import at.gravia.bird.exception.BirdException;
import at.gravia.bird.exception.ExceptionMessages;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Data
public class MailDto {
    private String to;
    private String from;
    private String cc;
    private String bcc;
    private String betreff;
    private String content;
    private boolean htmlContent;
    private List<Attachment> attachments;


    public boolean validate() {
        if (StringUtils.isBlank(to)) {
            throw new BirdException(ExceptionMessages.BIRD.MAIL_TO_NULL);
        }
        if (StringUtils.isBlank(from)) {
            throw new BirdException(ExceptionMessages.BIRD.MAIL_FROM_NULL);
        }
        if (StringUtils.isBlank(content)) {
            throw new BirdException(ExceptionMessages.BIRD.MAIL_CONTENT_NULL);
        }
        if (StringUtils.isBlank(betreff)) {
            throw new BirdException(ExceptionMessages.BIRD.MAIL_SUBJECT_NULL);
        }
        return true;
    }

    public void addAttachment(Attachment attachment) {
        if (this.attachments == null) {
            this.attachments = new ArrayList<>();
        }
        this.attachments.add(attachment);
    }
}
