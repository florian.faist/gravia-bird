package at.gravia.bird.data;

import lombok.Data;

import java.util.Base64;

@Data
public class Attachment {
    private String name;
    private String base64ByteArray;
    private boolean dataAsHtmlToPdf = false;

    public byte[] getData() {
        return Base64.getDecoder().decode(base64ByteArray.getBytes());
    }

    public String getDataAsString() {
        return new String(Base64.getDecoder().decode(base64ByteArray.getBytes()));
    }

    public void setData(byte[] object) {
        base64ByteArray = new String(Base64.getEncoder().encode(object));
    }
}
