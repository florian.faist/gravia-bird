package at.gravia.bird.data;

import lombok.Data;

@Data
public class HtmlDto {
    private String html;
}
