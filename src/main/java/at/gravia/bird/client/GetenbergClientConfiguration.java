package at.gravia.bird.client;

import com.google.gson.*;
import feign.Feign;
import feign.Logger;
import feign.form.FormEncoder;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class GetenbergClientConfiguration {

    @Value("${gravia.geteberg.serviceurl}")
    private String getebergServiceUrl;

    public GetenbergClientConfiguration() {

    }

    @Bean
    public GetenbergClientApi getBirdClient(GsonEncoder gsonEncoder,
                                            GsonDecoder gsonDecoder) {
        return Feign.builder()
                .client(new OkHttpClient())
                .encoder(new FormEncoder(gsonEncoder))
                .decoder(gsonDecoder)
                .logger(new Slf4jLogger(GetenbergClientApi.class))
                .logLevel(Logger.Level.BASIC)
                .target(GetenbergClientApi.class, getebergServiceUrl);
    }

    @Bean
    public GsonDecoder getGsonDecoder(Gson gson) {
        return new GsonDecoder(gson);
    }

    @Bean
    public GsonEncoder getGsonEncoder(Gson gson) {
        return new GsonEncoder(gson);
    }

    @Bean
    public Gson getGson() {
        return new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new JsonDeserializer<LocalDateTime>() {
            @Override
            public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

                return LocalDateTime.parse(json.getAsString(), DateTimeFormatter.ISO_DATE_TIME);
            }

        }).create();
    }
}
