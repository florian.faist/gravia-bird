package at.gravia.bird.client;


import feign.Headers;
import feign.Param;
import feign.RequestLine;
import feign.Response;

import java.io.File;

public interface GetenbergClientApi {

    @RequestLine("POST /convert/html")
    @Headers("Content-Type: multipart/form-data")
    Response createPdf(@Param("paperWidth") String paperWidth,
                       @Param("paperHeight") String paperHeight,
                       @Param("marginTop") String marginTop,
                       @Param("marginBottom") String marginBottom,
                       @Param("marginLeft") String marginLeft,
                       @Param("marginRight") String marginRight,
                       @Param("scale") String scale,
                       @Param("file") File file);
}
