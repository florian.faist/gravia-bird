package at.gravia.bird.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class InfoObj {
    private String type;
    private Object object;
}
