package at.gravia.bird.exception;

import java.time.LocalDateTime;
import java.util.Optional;

public class BirdException extends RuntimeException {
    protected ExceptionMsg exception;
    private final LocalDateTime time;
    private Exception parentException;

    public BirdException(ExceptionMsg message) {
        super(message.getMessageFormated());
        this.exception = message;
        this.time = LocalDateTime.now();
    }

    public BirdException(ExceptionMsg message, Exception parentException) {
        super(message.getMessageFormated());
        this.exception = message;
        this.time = LocalDateTime.now();
        this.parentException = parentException;
    }

    public ExceptionMsg getException() {
        return exception;
    }

    public String getCode() {
        return exception.getCode();
    }

    public String getType() {
        return exception.getType().name();
    }

    public LocalDateTime getTime() {
        return time;
    }

    public String getMessageFormated() {
        return this.exception.getMessageFormated();
    }

    public Optional<Exception> getParentException() {
        return Optional.ofNullable(parentException);
    }
}