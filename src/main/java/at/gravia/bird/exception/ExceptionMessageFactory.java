package at.gravia.bird.exception;

import java.util.HashMap;
import java.util.Map;

public class ExceptionMessageFactory {
    private static Map<String, ExceptionMsg> allExceptions = new HashMap<>();

    public static ExceptionMsg build(String code, String msg, ExceptionAffilation affilation, ExceptionType type) {
        ExceptionMsg exceptionMsg = new ExceptionMsg(code, msg, affilation, type);
        if (!allExceptions.containsKey(code)) {
            allExceptions.put(code, exceptionMsg);
            return exceptionMsg;
        } else {
            throw new IllegalArgumentException("This exception code was already used");
        }
    }

    public static ExceptionMsg findByCode(String code) {
        if (allExceptions.isEmpty()) {
            ExceptionMessages.init();
        }
        return allExceptions.get(code);
    }
}
