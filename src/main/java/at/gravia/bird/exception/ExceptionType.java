package at.gravia.bird.exception;

public enum ExceptionType {
    INFO,
    WARN,
    ERROR

}
