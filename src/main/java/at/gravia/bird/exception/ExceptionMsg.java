package at.gravia.bird.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExceptionMsg {
    private String code;
    private String msg;
    private String msgFormated;
    private ExceptionAffilation affilation;
    private ExceptionType type;
    private List<InfoObj> additionalInfo = new ArrayList<>();

    public ExceptionMsg(String code, String msg, ExceptionAffilation affilation, ExceptionType type) {
        this.code = code;
        this.msg = msg;
        this.affilation = affilation;
        this.type = type;

    }

    public String getMessageFormated() {
        if (this.msgFormated != null) {
            return type + ": - [" + code + "] " + msgFormated;
        }
        return type + ": - [" + code + "] " + msg;
    }

    public ExceptionMsg template(String... arg) {
        this.msgFormated = java.text.MessageFormat.format(this.msg, arg);
        return this;
    }

    public ExceptionMsg addInfo(Object... objects) {
        if (objects != null) {
            Arrays.asList(objects).stream().forEach(obj -> {
                if (obj instanceof InfoObj) {
                    additionalInfo.add((InfoObj) obj);
                } else {
                    additionalInfo.add(new InfoObj(obj.getClass().getSimpleName(), obj));
                }
            });

        }
        return this;
    }
}
