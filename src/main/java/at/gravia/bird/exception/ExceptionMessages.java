package at.gravia.bird.exception;


public class ExceptionMessages {
    public static void init() {
        System.out.println("heer");
    }

    public static class BIRD {
        private static final String DEF = "BIRD_";
        public static final ExceptionMsg OBJECT_NULL = ExceptionMessageFactory.build(DEF + "1", "Das Object {0} ist null", ExceptionAffilation.SYSTEM, ExceptionType.ERROR);
        public static final ExceptionMsg INVOICE_SEQUENCE_NULL = ExceptionMessageFactory.build(DEF + "2", "Die Rechnungsnummer konnte nicht ermittelt werden", ExceptionAffilation.SYSTEM, ExceptionType.ERROR);
        public static final ExceptionMsg MAIL_TO_NULL = ExceptionMessageFactory.build(DEF + "3", "Es muss ein empfänger angegeben werden", ExceptionAffilation.SYSTEM, ExceptionType.ERROR);
        public static final ExceptionMsg MAIL_FROM_NULL = ExceptionMessageFactory.build(DEF + "4", "Es muss ein sender angegeben werden", ExceptionAffilation.SYSTEM, ExceptionType.ERROR);
        public static final ExceptionMsg MAIL_CONTENT_NULL = ExceptionMessageFactory.build(DEF + "5", "Es muss ein mail inhalt angegeben werden", ExceptionAffilation.SYSTEM, ExceptionType.ERROR);
        public static final ExceptionMsg MAIL_SUBJECT_NULL = ExceptionMessageFactory.build(DEF + "6", "Es muss ein mail betreff angegeben werden", ExceptionAffilation.SYSTEM, ExceptionType.ERROR);
    }
}

