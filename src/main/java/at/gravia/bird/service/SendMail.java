package at.gravia.bird.service;

import at.gravia.bird.data.MailDto;
import com.googlecode.htmlcompressor.compressor.HtmlCompressor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Slf4j
@Service
public class SendMail {

    private JavaMailSender emailSender;

    @Value("#{'${mail.allowed.sender}'.split(';')}")
    private List<String> allowedSenderAdresses;

    public SendMail(@Autowired JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Async
    public void sendMail(MailDto mailDto) {
        try {
            if (allowedSenderAdresses.contains(mailDto.getFrom())) {
                MimeMessageHelper messageHelper = new MimeMessageHelper(emailSender.createMimeMessage(), MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                        StandardCharsets.UTF_8.name());
                if (mailDto.getAttachments() != null && !mailDto.getAttachments().isEmpty()) {
                    mailDto.getAttachments().forEach(attachment -> {
                        try {
                            messageHelper.addAttachment(attachment.getName(), new ByteArrayResource(attachment.getData()));
                        } catch (MessagingException e) {
                            log.error("Error during add Attachment", e);
                        }
                    });
                }
                if (mailDto.isHtmlContent()) {
                    System.out.println("SizeBefore Compress: " + mailDto.getContent().length());
                    HtmlCompressor htmlCompressor = new HtmlCompressor();
                    htmlCompressor.setCompressCss(true);
                    htmlCompressor.setPreserveLineBreaks(false);
                    mailDto.setContent(htmlCompressor.compress(mailDto.getContent()));
                    System.out.println("SizeAfter Compress: " + mailDto.getContent().length());
                }
                messageHelper.setText(mailDto.getContent(), mailDto.isHtmlContent()); // Use this or above line.
                messageHelper.setTo(mailDto.getTo());
                messageHelper.setSubject(mailDto.getBetreff());
                messageHelper.setFrom(mailDto.getFrom());
                emailSender.send(messageHelper.getMimeMessage());
                log.info("Es wurde eine mail an " + mailDto.getTo() + " mit dem betreff :" + mailDto.getBetreff() + " gesendet");
            } else {
                log.warn("Es wurde versucht eine mail von" + mailDto.getFrom() + " aus zu senden");
            }
        } catch (Exception e) {
            log.error("Error during Mail send", e);
        }
    }

}
