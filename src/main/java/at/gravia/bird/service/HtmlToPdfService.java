package at.gravia.bird.service;

import at.gravia.bird.client.GetenbergClientApi;
import at.gravia.bird.data.HtmlDto;
import com.google.common.io.ByteStreams;
import feign.Response;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Service
@Slf4j
public class HtmlToPdfService {
    private GetenbergClientApi getenbergClientApi;

    public HtmlToPdfService(GetenbergClientApi getenbergClientApi) {
        this.getenbergClientApi = getenbergClientApi;
    }

    public byte[] generatePDF(HtmlDto htmlToPdf) {
        String html = htmlToPdf.getHtml();
        try {
            html = StringUtils.replace(html, "<!DOCTYPE html>", "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
            html = StringUtils.replace(html, "<br>", "<br />");
            return generatePDF(html);
        } catch (Exception e) {
            log.error("Error during convert html to PDF ", e);
        }
        return null;
    }

    public byte[] generatePDFFromString(String htmlToPdf) {
        String html = htmlToPdf;
        try {
            html = StringUtils.replace(html, "<!DOCTYPE html>", "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
            html = StringUtils.replace(html, "<br>", "<br />");
            return generatePDF(html);
        } catch (Exception e) {
            log.error("Error during convert html to PDF ", e);
        }
        return null;
    }

    private synchronized byte[] generatePDF(String html) throws IOException {
        File tempFile = File.createTempFile("index", ".html");
        File fileToWork = new File(tempFile.getParent() + File.separator + "index.html");
        FileUtils.writeStringToFile(fileToWork, html, StandardCharsets.UTF_8);
        Response response = getenbergClientApi.createPdf("11.7", "16.5", "0.5", "0.5", "0", "0", "0.75", fileToWork);
        try {
            fileToWork.delete();
        } catch (Exception e) {
            log.error("Error during file index delete", e);
        }
        if (response != null && response.status() <= 200 && response.status() < 299) {
            try {
                return ByteStreams.toByteArray(response.body().asInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
